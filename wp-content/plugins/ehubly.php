<?php
/*
Plugin Name: Ehubly Link
Plugin URI:  http://www.vmax.vn
Description: Ehubly link
Version:     1.0
Author:      Sang Truong 
Author URI:  http://www.vmax.vn
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: /languages
Text Domain: ehubly-link
*/

function ehubly_post_type_link($url, $post = false, $leavename = false)
{
    $postId = null;
    if (!$post) {
        $postId = get_the_ID();
    } else {
        if (is_object($post) && isset($post->ID)) {
            $postId = $post->ID;
        }
    }
    $ehublyUrl = get_post_meta($postId, 'ehubly_link', true);
    if ($ehublyUrl) {
        return $ehublyUrl;
    } else {
        return $url;
    }
}
add_filter('post_link', 'ehubly_post_type_link', 10, 3);
add_filter('post_type_link', 'ehubly_post_type_link', 10, 3);

function ehubly_link_post_meta_register()
{
    $args = [
        'sanitize_callback' => 'sanitize_ehubly_link',
        'auth_callback'     => 'authorize_ehubly_link',
        'type'              => 'string',
        'description'       => 'eHubly Link',
        'single'            => true,
        'show_in_rest'      => true,
    ];
    register_meta('post', 'ehubly_link', $args);
}
add_action('init', 'ehubly_link_post_meta_register');
/*
function ehubly_link_post_type()
{
    register_post_type('ehubly_link', [
        'labels'      => [
            'name'          => __('Ehubly '),
            'singular_name' => __('Ehubly Link'),
        ],
        'public'      => true,
        'has_archive' => true,
    ]);
}
add_action('init', 'ehubly_link_post_type');


function ehubly_link_install() {
 
    // Trigger our function that registers the custom post type
    ehubly_link_post_type();
 
    // Clear the permalinks after the post type has been registered
    flush_rewrite_rules();
 
}
register_activation_hook( __FILE__, 'ehubly_link_install');


function ehubly_link_deactivation() {
 
    // Our post type will be automatically removed, so no need to unregister it
 
    // Clear the permalinks to remove our post type's rules
    flush_rewrite_rules();
 
}
register_deactivation_hook( __FILE__, 'ehubly_link_deactivation');
*/
