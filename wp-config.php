<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
/*
$connectstr_dbhost = 'v1.fullstackdeveloper.top';
$connectstr_dbname = 'v3_gocvanphong';
$connectstr_dbusername = 'root';
$connectstr_dbpassword = '123456';

foreach ($_SERVER as $key => $value) {
    if (strpos($key, "MMYSQLCONNSTR_localdb") !== 0) {
        continue;
    }
    
    $connectstr_dbhost = preg_replace("/^.*Data Source=(.+?);.*$/", "\\1", $value);
    $connectstr_dbname = preg_replace("/^.*Database=(.+?);.*$/", "\\1", $value);
    $connectstr_dbusername = preg_replace("/^.*User Id=(.+?);.*$/", "\\1", $value);
    $connectstr_dbpassword = preg_replace("/^.*Password=(.+?)$/", "\\1", $value);
}
*/

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'v3_gocvanphong');

/** MySQL database username */
define('DB_USER','root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
/**chang url */
define('WP_HOME','http://v1.fullstackdeveloper.top/');
define('WP_SITEURL','http://v1.fullstackdeveloper.top/');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-|@q-8{/#qJ~i2g_RG|:.03ky,hVBuhpL.dVj~oP>X<W.rVtBEc.`uk8_#`HhnJo');
define('SECURE_AUTH_KEY',  'V(g)H@:mP4k/(mj2aNw{=%bc /_x`y?W9%D3*tuCz@OvhR_of|GF:K|I>mk&6_.]');
define('LOGGED_IN_KEY',    'GE~[jP^WcVa7GiT}G^+#^t0|:K%rOO&3@/ohV/GE%j}+CNL.u*I|pKg3~%5b,]}4');
define('NONCE_KEY',        'Y/nJ}(Q}1a[vS(w?ddv12#-YoF+w5;_&ChQ}|w@5j03F10)lgr-oUvM!_ozz0Km%');
define('AUTH_SALT',        'jP>.^bKqPS;G>[L)qy<-|%ReU.`_-?eT;}S|l7)Cs9<;m*t|ZH r,ySl}#tjET&O');
define('SECURE_AUTH_SALT', 'vd/,&^YWZ8,!{HgOZ]He$@1?i`BiTYc6}I2-KvVlN6~sti1UEz>S,={HSBuf&]`L');
define('LOGGED_IN_SALT',   'FV04_Q{ru&D|86-1cdPWj2|~WSnd@_,%q4(;OR;Dx(!jx(pLJo :Pl{/ATuA{zEZ');
define('NONCE_SALT',       'fwY%$Z%gmnI+f4M-Te4d/-~<l(+F1_; xQehTS7^J3^z7VhZ/2mh[QDaY/6y%z`S');
define('WP_ALLOW_REPAIR', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
